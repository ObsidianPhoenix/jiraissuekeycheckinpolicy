@echo off
set /p version="Version number: "

pushd "VSIXPackage - VS2010"
msbuild /t:UpdateVersion /property:artifactVersion=%version%
popd

pushd "VSIXPackage - VS2012"
msbuild /t:UpdateVersion /property:artifactVersion=%version%
popd

pushd "VSIXPackage - VS2013"
msbuild /t:UpdateVersion /property:artifactVersion=%version%
popd

pushd "VSIXPackage - VS2015"
msbuild /t:UpdateVersion /property:artifactVersion=%version%
popd

pushd "VSIXPackage - VS2017"
msbuild /t:UpdateVersion /property:artifactVersion=%version%
popd