﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spartez.TFS4JIRA.CheckInPolicy.Settings
{
    public class SettingsViewModel : INotifyPropertyChanged
    {
        private string _errorMessage, _successMessage;
        private bool _allowEdit;
        private bool _allowEditJiraUrl;
        private IEnumerable<string> _checkInNoteNames;
        private bool _scanCheckInComment;
        private bool _scanCheckInNote;
        private string _scannedCheckInNoteName;

        public SettingsViewModel()
        {
            _errorMessage = "";
            _successMessage = "";
            _allowEdit = true;
            _allowEditJiraUrl = true;
            _checkInNoteNames = new String[] {};
        }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                if (value == _errorMessage)
                    return;
                _errorMessage = value;
                OnPropertyChanged("ErrorMessage");
            }
        }

        public string SuccessMessage
        {
            get { return _successMessage; }
            set
            {
                if (value == _successMessage)
                    return;
                _successMessage = value;
                OnPropertyChanged("SuccessMessage");
            }
        }

        public bool AllowEdit
        {
            get { return _allowEdit; }
            set
            {
                if (value == _allowEdit)
                    return;
                _allowEdit = value;
                OnPropertyChanged("AllowEdit");
            }
        }

        public bool AllowEditJiraUrl
        {
            get { return _allowEditJiraUrl; }
            set
            {
                if (value == _allowEditJiraUrl)
                    return;
                _allowEditJiraUrl = value;
                OnPropertyChanged("AllowEditJiraUrl");
            }
        }

        public bool ScanCheckInNote
        {
            set
            {
                _scanCheckInNote = value;
                OnPropertyChanged("EnableOkButton");
                OnPropertyChanged("ScanCheckInNote");
            }
            get { return _scanCheckInNote; }
        }

        public bool ScanCheckInComment
        {
            set
            {
                _scanCheckInComment = value;
                OnPropertyChanged("EnableOkButton");
                OnPropertyChanged("ScanCheckInComment");
            }

            get { return _scanCheckInComment; }
        }

        public string ScannedCheckInNoteName
        {
            set
            {
                _scannedCheckInNoteName = value;
                OnPropertyChanged("EnableOkButton");
                OnPropertyChanged("ScannedCheckInNoteName");
            }

            get { return _scannedCheckInNoteName; }
        }

        public bool EnableOkButton
        {
            get { return _scanCheckInComment || (_scanCheckInNote && !String.IsNullOrEmpty(_scannedCheckInNoteName)); }
        }

        public IEnumerable<string> CheckInNoteNames
        {
            get { return _checkInNoteNames; }
            set
            {
                if (value == _checkInNoteNames)
                    return;
                _checkInNoteNames = value;
                OnPropertyChanged("CheckInNoteNames");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
