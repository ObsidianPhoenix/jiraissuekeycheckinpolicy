﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Navigation;


using System.ComponentModel;
using Spartez.TFS4JIRA.CheckInPolicy.JIRA;
using Spartez.TFS4JIRA.CheckInPolicy.PolicyHelper;

namespace Spartez.TFS4JIRA.CheckInPolicy.Settings
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsViewModel SettingsViewModel { get; private set; }
        
        public LocalCheckInPolicySettings LocalSettings { get; private set; }
        public GlobalCheckInPolicySettings GlobalSettings { get; private set; }

        public SettingsWindow(GlobalCheckInPolicySettings globalCheckInPolicySettings, LocalCheckInPolicySettings localCheckInPolicySettings, IEnumerable<string> checkInNoteNames, bool allowEditJiraUrl)
        {        
            InitializeComponent();
            this.ShowInTaskbar = false;

            this.LocalSettings = localCheckInPolicySettings;
            this.GlobalSettings = globalCheckInPolicySettings;           

            this.JiraUrl.Text = localCheckInPolicySettings.JiraUrl;
            this.Login.Text = localCheckInPolicySettings.Login;
            this.Password.Password = localCheckInPolicySettings.Password;

            string[] checkinNoteNameComboboxOptions = checkInNoteNames.ToArray();

            this.SettingsViewModel = new SettingsViewModel { AllowEditJiraUrl = allowEditJiraUrl };

            if (checkinNoteNameComboboxOptions.Length == 0)
            {
                checkinNoteNameComboboxOptions = new[] { " -- No check-in notes defined --" };

                this.SettingsViewModel.ScanCheckInNote = false;
                this.SettingsViewModel.ScannedCheckInNoteName = checkinNoteNameComboboxOptions[0];
            }
            else if (!checkinNoteNameComboboxOptions.Contains(globalCheckInPolicySettings.ScannedCheckInNoteName))
            {
                this.SettingsViewModel.ScanCheckInNote = globalCheckInPolicySettings.ScanCheckInNote;
                this.SettingsViewModel.ScannedCheckInNoteName = checkinNoteNameComboboxOptions[0];
            }
            else
            {
                this.SettingsViewModel.ScanCheckInNote = globalCheckInPolicySettings.ScanCheckInNote;
                this.SettingsViewModel.ScannedCheckInNoteName = globalCheckInPolicySettings.ScannedCheckInNoteName;    
            }

            this.SettingsViewModel.CheckInNoteNames = checkinNoteNameComboboxOptions;

            this.SettingsViewModel.ScanCheckInComment = globalCheckInPolicySettings.ScanCheckInComment;

            this.DataContext = this.SettingsViewModel;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            LocalSettings.JiraUrl = this.JiraUrl.Text;
            LocalSettings.Login = this.Login.Text;
            LocalSettings.Password = this.Password.Password;

            GlobalSettings.JiraUrl = this.JiraUrl.Text;
            GlobalSettings.ScanCheckInComment = (this.ScanCheckInComment.IsChecked == true);
            GlobalSettings.ScanCheckInNote = (this.ScanCheckInNote.IsChecked == true);
            GlobalSettings.ScannedCheckInNoteName = (String)this.ScannedCheckInNoteName.SelectedItem;

            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void TestButton_Click(object sender, RoutedEventArgs e)
        {
            SettingsViewModel.AllowEdit = false;
            SettingsViewModel.ErrorMessage = "";
            SettingsViewModel.SuccessMessage = "";

            pictureBoxLoading.Image = Properties.Resources.Animation;

            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += (s, ev) => 
            {
                LocalCheckInPolicySettings settingsModel = (LocalCheckInPolicySettings)ev.Argument;
                JiraConnection jiraConnection = new JiraConnection(settingsModel);
                ev.Result = jiraConnection.TestConnection();
            };

            bw.RunWorkerCompleted += (s, ev) =>
            {
                SettingsViewModel.ErrorMessage = (String)ev.Result;
                SettingsViewModel.SuccessMessage = (ev.Result == null) ? "Successfully connected to JIRA server" : "";

                SettingsViewModel.AllowEdit = true;
                pictureBoxLoading.Image = null;
            };

            bw.RunWorkerAsync(new LocalCheckInPolicySettings()
            {
                JiraUrl = JiraUrl.Text,
                Login = Login.Text,
                Password = Password.Password
            });
        }

        private void Hyperlink_OnRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo("explorer.exe", Properties.Resources.WikiHomeUrl));
        }
    }
}
